﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MvcMusicStore.Dependencies;
using MvcMusicStore.Services;
using MvcMusicStore.MongoDB;
using MvcMusicStore.Services.Account;
using MvcMusicStore.Services.Home;
using MvcMusicStore.Services.InitialMongoDB;
using MvcMusicStore.Services.Store;
using Ninject;

namespace MvcMusicStore
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            //Вот пример регистрации объекта класса MongoDocumentsDatabase как Singleton с ипользованием StructureMap (аналог NInject):
            //container.Configure(config => config.For<MongoDocumentsDatabase>().Singleton().Use(new MongoDocumentsDatabase(“mongodb://localhost:27002”)));

            //Вот как сделать тоже самое на NInject:
            //kernel.Bind<MongoDocumentsDatabase>().ToConstant(new MongoDocumentsDatabase(“mongodb://localhost:27002”)).InSingletonScope();

            IKernel kernel = new StandardKernel();
            kernel.Bind<MongoDocumentsDatabase>().ToConstant(
                new MongoDocumentsDatabase(
                    "mongodb://Yegor_:irjh1990@ds047427.mongolab.com:47427/mongo_music_store_db")).
                InSingletonScope();

            /* isn't required
            kernel.Bind<InitialMongoDBService>().To<InitialMongoDBService>();
            kernel.Bind<AccountService>().To<AccountService>();
            kernel.Bind<HomeService>().To<HomeService>();
            kernel.Bind<StoreService>().To<StoreService>(); */

            DependencyResolver.SetResolver(new StoreDependencyResolver(kernel));
            
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}