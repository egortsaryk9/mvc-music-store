﻿using MongoDB.Bson.Serialization.Attributes;

namespace MvcMusicStore.Documents
{
    public class AlbumDocument
    {
        [BsonId]
        public string Id { get; set; }
        public string Title { get; set; }
        public string GenreName { get; set; }
        public string ArtistName { get; set; }
        public float Price { get; set; }
        public string AlbumArtUrl { get; set; }
    }
}