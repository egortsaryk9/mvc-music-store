﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;

namespace MvcMusicStore.Documents
{
    public class CartDocument
    {
        [BsonId]
        public string Id { get; set; }
        public string UserName { get; set; }
        public IList<AlbumDocument> Purchase { get; set; }
    }
}