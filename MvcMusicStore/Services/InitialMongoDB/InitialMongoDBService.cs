﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MvcMusicStore.Documents;
using MvcMusicStore.MongoDB;

namespace MvcMusicStore.Services.InitialMongoDB
{
    public class InitialMongoDBService
    {
        private MongoDocumentsDatabase _database;

        public InitialMongoDBService(MongoDocumentsDatabase database)
        {
            this._database = database;
        }

        public void SetAlbumsToMongo()
        {
            _database.Albums.InsertBatch(albums);
        }

         AlbumDocument[] albums = new AlbumDocument[]
                                         {
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Deep Purple",
                                                     Title = "Deep Purple in Rock",
                                                     GenreName = "Rock",
                                                     Price = 20,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Accept",
                                                     Title = "Balls to the Wall",
                                                     GenreName = "Rock",
                                                     Price = 15,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Iron Maiden",
                                                     Title = "Brave new world",
                                                     GenreName = "Metal",
                                                     Price = 30,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "UB40",
                                                     Title = "UB40 The Best Of - Volume Two [UK]",
                                                     GenreName = "Reggae",
                                                     Price = 150,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Spyro Gyra",
                                                     Title = "Heart of the Night",
                                                     GenreName = "Jazz",
                                                     Price = 11,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Calexico",
                                                     Title = "Carried to Dust (Bonus Track Version)",
                                                     GenreName = "Alternative",
                                                     Price = 12,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Eric Clapton",
                                                     Title = "The Cream Of Clapton",
                                                     GenreName = "Blues",
                                                     Price = 13,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Anita Ward",
                                                     Title = "Ring My Bell",
                                                     GenreName = "Disco",
                                                     Price = 17,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Amy Winehouse",
                                                     Title = "Frank",
                                                     GenreName = "Pop",
                                                     Price = 13,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Eugene Ormandy",
                                                     Title = "Respighi:Pines of Rome",
                                                     GenreName = "Classical",
                                                     Price = 15,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 },
                                             new AlbumDocument()
                                                 {
                                                     ArtistName = "Falamansa",
                                                     Title = "Deixa Entrar",
                                                     GenreName = "Latin",
                                                     Price = 18,
                                                     AlbumArtUrl = "/Content/Images/placeholder.gif",
                                                     Id = GetAlbumId()
                                                 }
                                         };
        
        public static string GetAlbumId()
        {
            return ObjectId.GenerateNewId().ToString();
        }
        
    }
}