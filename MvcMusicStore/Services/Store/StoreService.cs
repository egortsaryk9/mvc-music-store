﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Models;
using MvcMusicStore.MongoDB;
using MvcMusicStore.ViewModels.StoreViewModels;

namespace MvcMusicStore.Services.Store
{
    public class StoreService
    {
        private readonly AlbumsDocumentsService _albums;

        public StoreService(AlbumsDocumentsService albums)
        {
            this._albums = albums;
        }

        //
        // Index Action
        public IEnumerable<StoreIndexViewModel> GetIndexViewModel()
        {
            var indexViewModel = new List<StoreIndexViewModel>();
            
            foreach (var genre in Genres)
            {
                var viewModel = new StoreIndexViewModel(genre);
                indexViewModel.Add(viewModel);
            }

            return indexViewModel;
        }

        //
        // Browse Action
        public StoreBrowseViewModel GetBrowseViewModel(string name)
        {
            var albumsOfGenre = _albums.GetByFilter(new AlbumFilter() { GenreName = name });
            var albumsViewModel = new List<StoreBrowseAlbumViewModel>();
            
            foreach (var album in albumsOfGenre)
            {
                var albumViewModel = new StoreBrowseAlbumViewModel(album);
                albumsViewModel.Add(albumViewModel);
            }

            var browseViewModel = new StoreBrowseViewModel(name, albumsViewModel);
            return browseViewModel;
        }

        //
        // Details Action
        public StoreDetailsViewModel GetDetailsViewModel(string id)
        {
            var album = this._albums.GetAlbumById(id);
            var detailsViewModel = new StoreDetailsViewModel(album);
            return detailsViewModel;
        }

        //
        // GenreMenu Action
        public IEnumerable<StoreGenreMenuViewModel> GetGenreMenuViewModel()
        {
            var genreMenuViewModel = new List<StoreGenreMenuViewModel>();
            foreach (var genre in Genres)
            {
                var genreViewModel = new StoreGenreMenuViewModel(genre);
                genreMenuViewModel.Add(genreViewModel);
            }

            return genreMenuViewModel;
        }

        //
        // All genres 
        public static readonly string[] Genres = new string[]
                                                     {
                                                         "Rock",
                                                         "Jazz",
                                                         "Disco",
                                                         "Pop",
                                                         "Classical",
                                                         "Reggae",
                                                         "Blues",
                                                         "Latin",
                                                         "Alternative",
                                                         "Metal"
                                                     };
    }
}