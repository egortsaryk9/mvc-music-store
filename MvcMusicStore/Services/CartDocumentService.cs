﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MvcMusicStore.Documents;
using MvcMusicStore.Models;
using MvcMusicStore.MongoDB;

namespace MvcMusicStore.Services
{
    public class CartDocumentService : DocumentsServiceFiltered<CartDocument, CartFilter>
    {
        public CartDocumentService(MongoDocumentsDatabase database)
            : base(database)
        {

        }

        protected override MongoCollection<CartDocument> Items
        {
            get { return Database.Carts; }
        }

        protected override IEnumerable<IMongoQuery> BuildFilterQuery(CartFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.UserName))
            {
                yield return Query<CartDocument>.EQ(x => x.UserName, filter.UserName);
            }

            if (!string.IsNullOrEmpty(filter.Id))
            {
                yield return Query<CartDocument>.EQ(x => x.Id, filter.Id);
            }
        }

        public CartDocument GetCartByUserName(string name)
        {
            return Items.FindOne(Query<CartDocument>.EQ(x => x.UserName, name));

        }

        public void ClearCart(string name)
        {
            //wrong
            //Items.FindOne(Query<CartDocument>.EQ(x => x.UserName, name)).Purchase.Clear();
            
            var userCart = Items.FindOne(Query<CartDocument>.EQ(x => x.UserName, name));
            userCart.Purchase.Clear();
            //Items.Update(Query<CartDocument>.EQ(x => x.UserName, name),
            //             Update<CartDocument>.Set(x => x.Purchase, new List<AlbumDocument>()));
            Items.Save(userCart);
        }

        /* wrong alternative
        public void RemoveAlbumFromCart(string name, string id)
        {
            Items.FindOne(Query<CartDocument>.EQ(x => x.UserName, name)).Purchase.Remove(Database.Albums.FindOneById(id));
        }
        */
    }

    public class CartFilter : BaseFilter
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }

}