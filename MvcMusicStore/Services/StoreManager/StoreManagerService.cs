﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using MvcMusicStore.Documents;
using MvcMusicStore.ViewModels.StoreManagerViewModels;

namespace MvcMusicStore.Services.StoreManager
{
    public class StoreManagerService
    {

        private AlbumsDocumentsService _albums;

        public StoreManagerService(AlbumsDocumentsService albums)
        {
            this._albums = albums;
        }

        public IEnumerable<StoreManagerIndexViewModel> GetIndexViewModel()
        {
            var allAlbums = this._albums.GetAll();
            var listOfViewModels = new List<StoreManagerIndexViewModel>();
            
            foreach (var album in allAlbums)
            {
                var albumViewModel = new StoreManagerIndexViewModel(album);
                listOfViewModels.Add(albumViewModel);
            }

            return listOfViewModels;
        }

        public StoreManagerDetailsViewModel GetDetailsViewModel(string id)
        {
            var albumViewModel = new StoreManagerDetailsViewModel(this._albums.GetAlbumById(id));
            return albumViewModel;
        }

        public StoreManagerCreateViewModel GetCreateViewModel()
        {
            SelectList listOfGenres = new SelectList(Genres, "GenreName");
            SelectList listOfArtists = new SelectList(Artists, "ArtistName");

            var createViewModel = new StoreManagerCreateViewModel()
                                      {
                                          ListOfGenres = listOfGenres,
                                          ListOfArtists = listOfArtists
                                      };
            return createViewModel;
        }

        public void CreateNewAlbum(StoreManagerCreateViewModel dataForCreating)
        {
            var newAlbum = new AlbumDocument()
                               {
                                   Title = dataForCreating.Title,
                                   ArtistName = dataForCreating.ArtistName,
                                   GenreName = dataForCreating.GenreName,
                                   AlbumArtUrl = dataForCreating.AlbumArtUrl,
                                   Price = dataForCreating.Price,
                                   Id = GetAlbumId()
                               };

            this._albums.Insert(newAlbum);
        }

        public StoreManagerEditViewModel GetEditViewModel(string id)
        {
            var editingAlbum = this._albums.GetAlbumById(id);

            SelectList listOfGenres = new SelectList(Genres, "GenreName");
            SelectList listOfArtists = new SelectList(Artists, "ArtistName");

            var editViewModel = new StoreManagerEditViewModel()
                                    {
                                        Title = editingAlbum.Title,
                                        ArtistName = editingAlbum.ArtistName,
                                        GenreName = editingAlbum.GenreName,
                                        Price = editingAlbum.Price,
                                        AlbumArtUrl = editingAlbum.AlbumArtUrl,
                                        Id = editingAlbum.Id,

                                        ListOfGenres = listOfGenres,
                                        ListOfArtists = listOfArtists
                                    };
            return editViewModel;
        }


        public void EditAlbum(StoreManagerEditViewModel dataForUpdating)
        {
            var editedAlbum = this._albums.GetAlbumById(dataForUpdating.Id);
            editedAlbum.Title = dataForUpdating.Title;
            editedAlbum.ArtistName = dataForUpdating.ArtistName;
            editedAlbum.GenreName = dataForUpdating.GenreName;
            editedAlbum.Price = dataForUpdating.Price;
            editedAlbum.AlbumArtUrl = dataForUpdating.AlbumArtUrl;

            this._albums.Save(editedAlbum);
        }

        public StoreManagerDeleteViewModel GetDeleteViewModel(string id)
        {
            var deleteViewModel = new StoreManagerDeleteViewModel(this._albums.GetAlbumById(id).Title);
            return deleteViewModel;
        }

        public void ConfirmDelete(string id)
        {
            this._albums.DeleteAlbum(id);
        }

        public static readonly string[] Genres = new string[]
                                                     {
                                                         "Rock",
                                                         "Jazz",
                                                         "Disco",
                                                         "Pop",
                                                         "Classical",
                                                         "Reggae",
                                                         "Blues",
                                                         "Latin",
                                                         "Alternative",
                                                         "Metal"
                                                     };

        public static readonly string[] Artists = new string[]
                                                     {
                                                         "Deep Purple",
                                                         "Accept",
                                                         "Iron Maiden",
                                                         "UB40",
                                                         "Spyro Gyra",
                                                         "Calexico",
                                                         "Eric Clapton",
                                                         "Anita Ward",
                                                         "Amy Winehouse",
                                                         "Eugene Ormandy",
                                                         "Falamansa"
                                                     };

        public static string GetAlbumId()
        {
            return ObjectId.GenerateNewId().ToString();
        }
    }
}