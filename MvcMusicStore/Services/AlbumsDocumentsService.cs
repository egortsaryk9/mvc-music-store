﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using MvcMusicStore.Documents;
using MvcMusicStore.MongoDB;

namespace MvcMusicStore.Services
{
    public class AlbumsDocumentsService: DocumentsServiceFiltered<AlbumDocument,AlbumFilter>
    {
        public AlbumsDocumentsService(MongoDocumentsDatabase database) : base(database)
        {
        }

        protected override MongoCollection<AlbumDocument> Items
        {
            get { return Database.Albums; }
        }


        // Example of usage - method GetByFilter(new AlbumFilter{MaxPrice = 10.00})
        protected override IEnumerable<IMongoQuery> BuildFilterQuery(AlbumFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.GenreName))
            {
                yield return Query<AlbumDocument>.EQ(x => x.GenreName, filter.GenreName);
            }
            if (!string.IsNullOrEmpty(filter.ArtistName))
            {
                yield return Query<AlbumDocument>.EQ(x => x.ArtistName, filter.ArtistName);
            }
            //if filtered by min price
            if (filter.MinPrice.HasValue)
            {
                //GT = Greater Then, GTE = Greater Then or Equal
                yield return Query<AlbumDocument>.GTE(x => x.Price, filter.MinPrice.Value);
            }
            //if filtered by max price
            if (filter.MaxPrice.HasValue)
            {
                //LT = Less Then, LTE = Less Then or Equal
                yield return Query<AlbumDocument>.LTE(x => x.Price, filter.MaxPrice.Value);
            }
        }

        public IEnumerable<AlbumDocument> GetTop10CheapestAlbums()
        {
            return Items.FindAll().SetSortOrder(SortBy<AlbumDocument>.Ascending(x => x.Price)).SetLimit(10);
        }

        //Absolutely the same as previous but with familiar to you Linq and IQueryble
        public IEnumerable<AlbumDocument> GetTop10CheapestAlbums2()
        {
            return (from album in Items.AsQueryable() orderby album.Price select album).Take(10);
        }

        public IEnumerable<AlbumDocument> GetTopAlbums(int amount)
        {
            return Items.FindAll().SetSortOrder(SortBy<AlbumDocument>.Descending(x => x.Price)).SetLimit(amount);
        }

        public AlbumDocument GetByPrice(float price)
        {
            return Items.FindOne(Query<AlbumDocument>.EQ(x => x.Price, price));
        }

        public AlbumDocument GetAlbumById(string id)
        {
            return Items.FindOne(Query<AlbumDocument>.EQ(x => x.Id, id));
        }

        public void DeleteAlbum(string id)
        {
            Items.Remove(Query<AlbumDocument>.EQ(x => x.Id, id));
        }
    }

    public class AlbumFilter : BaseFilter
    {
        public string GenreName { get; set; }
        public string ArtistName { get; set; }
        public float? MinPrice { get; set; }
        public float? MaxPrice { get; set; }
    }
}