﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using Ninject.Parameters;

namespace MvcMusicStore.Dependencies
{
    public class StoreDependencyResolver : IDependencyResolver
    {
        private IKernel _kernel;

        public StoreDependencyResolver(IKernel kernel)
        {
            this._kernel = kernel;
        }
        
        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType, new IParameter[0]);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType, new IParameter[0]);
        }
    }
}