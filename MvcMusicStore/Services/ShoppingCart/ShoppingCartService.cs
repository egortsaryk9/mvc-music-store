﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MvcMusicStore.Documents;
using MvcMusicStore.ViewModels;
using MvcMusicStore.ViewModels.ShoppingCartViewModels;

namespace MvcMusicStore.Services.ShoppingCart
{
    public class ShoppingCartService
    {
        private CartDocumentService _carts;
        private AlbumsDocumentsService _albums;
        
        public ShoppingCartService(CartDocumentService carts, AlbumsDocumentsService albums)
        {
            this._carts = carts;
            this._albums = albums;
        }

        public ShoppingCartIndexViewModel GetIndexViewModel(string name)
        {
            var userCart = _carts.GetCartByUserName(name);
            var allPurchases = userCart.Purchase;
            var listOfViewModels = new List<ShoppingCartViewModel>();

            foreach (var purchase in allPurchases)
            {
                var cartViewModel = new ShoppingCartViewModel(purchase);

                listOfViewModels.Add(cartViewModel);
            }

            var indexViewModel = new ShoppingCartIndexViewModel(listOfViewModels, allPurchases);
            return indexViewModel;

        }

        public void AddToCart(string id, string name)
        {
            //wrong
            //----------------------------------------------
            //var wrongAlbum = this._albums.GetAlbumById(id);
            //----------------------------------------------

            var userCart = this._carts.GetCartByUserName(name);
            userCart.Purchase.Add(_albums.GetAlbumById(id));
            _carts.Save(userCart);
        }

        public ShoppingCartRemoveViewModel RemoveFromCart(string id, string name)
        {
            //wrong
            //---------------------------------------------
            //var wrongAlbum = this._albums.GetAlbumById(id);
            //---------------------------------------------

            var userCart = this._carts.GetCartByUserName(name);
            var deletedAlbum = userCart.Purchase.First(x => x.Id == id);
            userCart.Purchase.Remove(deletedAlbum);

            //wrong
            //------------------------------------
            //userCart.Purchase.Remove(wrongAlbum);
            //------------------------------------ 

            //this._carts.RemoveAlbumFromCart(name, id);<-- wrong alternative

            _carts.Save(userCart);
            var removeViewModel = new ShoppingCartRemoveViewModel(userCart, deletedAlbum);
            return removeViewModel;

        }

        public ShoppingCartSummaryViewModel GetCartSummaryViewModel(string name)
        {
            var summaryViewModel = new ShoppingCartSummaryViewModel(this._carts.GetCartByUserName(name));
            return summaryViewModel;
        }
        
        public ShoppingCartSummaryViewModel GetEmptyCart()
        {
            var emptyCart = new CartDocument()
                                {
                                    Purchase = new List<AlbumDocument>()
                                };
            var emptyCartViewModel = new ShoppingCartSummaryViewModel(emptyCart);
            return emptyCartViewModel;
        }
    }
}
