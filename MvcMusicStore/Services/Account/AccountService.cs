﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MvcMusicStore.Documents;
using MvcMusicStore.Models;
using MvcMusicStore.MongoDB;

namespace MvcMusicStore.Services.Account
{
    public class AccountService
    {
        private CartDocumentService _carts;

        public AccountService(CartDocumentService carts)
        {
            this._carts = carts;
        }

        public void CreateUserCartByName(string name)
        {
            var newUserCart = new CartDocument()
                                        {
                                            UserName = name,
                                            Purchase = new List<AlbumDocument>(),
                                            Id = GetIdForCart()
                                        };

            this._carts.Insert(newUserCart);
        }

        public CartDocument GetCartByUserName(string name)
        {
            return _carts.GetCartByUserName(name);
        }

        public static string GetIdForCart()
        {
            return ObjectId.GenerateNewId().ToString();
        }
    }
}