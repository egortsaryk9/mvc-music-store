﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;
using MvcMusicStore.MongoDB;
using MvcMusicStore.ViewModels.HomeViewModels;

namespace MvcMusicStore.Services.Home
{
    public class HomeService
    {
        private readonly AlbumsDocumentsService _albums;

        public HomeService(AlbumsDocumentsService albums)
        {
            this._albums = albums;
        }

        //
        // Index Action
        public IEnumerable<HomeIndexViewModel> GetIndexViewModel()
        {
            var topAlbums = this._albums.GetTopAlbums(5);
            var topAlbumsViewModels = new List<HomeIndexViewModel>();
            foreach (var topAlbum in topAlbums)
            {
                var topAlbumViewModel = new HomeIndexViewModel(topAlbum);
                topAlbumsViewModels.Add(topAlbumViewModel);
            }

            return topAlbumsViewModels;
        }
    }
}