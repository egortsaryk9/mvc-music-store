﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MvcMusicStore.Documents;
using MvcMusicStore.MongoDB;

namespace MvcMusicStore.Services
{
    public class OrderDocumentService : DocumentsServiceFiltered<OrderDocument, OrderFilter>
    {
        public OrderDocumentService(MongoDocumentsDatabase database)
            : base(database)
        {
        }
        protected override MongoCollection<OrderDocument> Items
        {
            get { return Database.Orders; }
        }

        protected override IEnumerable<IMongoQuery> BuildFilterQuery(OrderFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.UserName))
            {
                yield return Query<OrderDocument>.EQ(x => x.UserName, filter.UserName);
            }

            if (!string.IsNullOrEmpty(filter.Id))
            {
                yield return Query<OrderDocument>.EQ(x => x.Id, filter.Id);
            }
        }

        public OrderDocument CheckOrder(string id)
        {
            return Items.FindOne(Query<OrderDocument>.EQ(x => x.Id, id));
        }
    }

    public class OrderFilter : BaseFilter
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime OrderDate { get; set; }
    }
}