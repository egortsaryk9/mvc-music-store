﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using MongoDB.Bson;
using MvcMusicStore.Documents;
using MvcMusicStore.ViewModels.CheckoutViewModels;

namespace MvcMusicStore.Services.Checkout
{
    public class CheckoutService
    {
        private CartDocumentService _carts;
        private OrderDocumentService _orders;

        public CheckoutService(CartDocumentService carts, OrderDocumentService orders)
        {
            this._carts = carts;
            this._orders = orders;
        }

        public int CheckContent(string name)
        {
            var chekingCart = this._carts.GetCartByUserName(name);
            int check = 0;
            foreach (var purchase in chekingCart.Purchase)
            {
                check += 1;
            }
            
            return check;
        }

        public string MakeOrder(CheckoutAdressAndPaymentViewModel dataForOrder, string userName)
        {
            var newOrder = new OrderDocument()
                               {
                                   Id = GetOrderId(),
                                   FirstName = dataForOrder.FirstName,
                                   LastName = dataForOrder.LastName,
                                   Adress = dataForOrder.Address,
                                   City = dataForOrder.City,
                                   State = dataForOrder.State,
                                   PostalCode = dataForOrder.PostalCode,
                                   Country = dataForOrder.Country,
                                   Phone = dataForOrder.Phone,
                                   Email = dataForOrder.Email,
                                   UserName = userName,
                                   OrderDate = DateTime.Now
                               };
            
            this._orders.Insert(newOrder);

            // wrong
            // this._carts.GetCartByUserName(userName).Purchase.Clear();
            //
            
            this._carts.ClearCart(userName);
            return newOrder.Id;
            
        }

        public OrderDocument CheckOrder(string id)
        {
            return this._orders.CheckOrder(id);
        }

        public static string GetOrderId()
        {
            return ObjectId.GenerateNewId().ToString();
        }

    }
}