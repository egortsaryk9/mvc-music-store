﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using MvcMusicStore.Documents;
using MvcMusicStore.MongoDB;
using MvcMusicStore.Services.InitialMongoDB;

namespace MvcMusicStore.Controllers
{
    [Authorize]
    public class InitialMongoDBController : Controller
    {
        private InitialMongoDBService _service;

        public InitialMongoDBController(InitialMongoDBService service)
        {
            this._service = service;
        }

        //
        // GET: /InitialMongoDB/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InitialAlbums()
        {
            _service.SetAlbumsToMongo();
            return RedirectToAction("Index", "Home");
        }

    }
}
