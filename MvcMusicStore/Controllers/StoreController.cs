﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMusicStore.Models;
using MvcMusicStore.Services.Store;
using MvcMusicStore.ViewModels.StoreViewModels;

namespace MvcMusicStore.Controllers
{
    public class StoreController : Controller
    {
        private StoreService _service;

        public StoreController(StoreService service)
        {
            this._service = service;
        }


        public ActionResult Index()
        {
            var indexViewModel = this._service.GetIndexViewModel();
            return View(indexViewModel);
        }


        public ActionResult Browse(string genre)
        {
            var browseViewModel = this._service.GetBrowseViewModel(genre);
            return View(browseViewModel);
        }


        public ActionResult Details(string id)
        {
            var detailsViewModel = this._service.GetDetailsViewModel(id);
            return View(detailsViewModel);
        }


        [ChildActionOnly]
        public ActionResult GenreMenu()
        {
            var genresViewModel = this._service.GetGenreMenuViewModel();
            return PartialView(genresViewModel);
        }
    }
}
