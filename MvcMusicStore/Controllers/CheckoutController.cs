﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMusicStore.Models;
using MvcMusicStore.Services.Checkout;
using MvcMusicStore.ViewModels.CheckoutViewModels;

namespace MvcMusicStore.Controllers
{
    [Authorize]
    public class CheckoutController : Controller
    {
        private CheckoutService _service;

        public CheckoutController(CheckoutService service)
        {
            this._service = service;
        }

        private const string PromoCode = "FREE";


        [HttpGet]
        public ActionResult AdressAndPayment()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult AdressAndPayment(CheckoutAdressAndPaymentViewModel dataForOrder)
        {
            var cartContent = this._service.CheckContent(this.HttpContext.User.Identity.Name);
            if (cartContent == 0)
            {
                return RedirectToAction("AdressAndPayment");
            }

            try
            {
                if (string.Equals(dataForOrder.PromoCode, PromoCode, StringComparison.OrdinalIgnoreCase) == false)
                {
                    return View(dataForOrder);
                }

                else
                {
                    var idForCompleting = this._service.MakeOrder(dataForOrder, this.HttpContext.User.Identity.Name);
                    return RedirectToAction("Complete", new { id = idForCompleting });
                }
            }
            catch
            {
                // Invalid - display with errors
                return View(dataForOrder);
            }
        }

       
        public ActionResult Complete(string id)
        {
            // Vaidate customer owns this order
            try
            {
                var order = this._service.CheckOrder(id);
                var numberOfOrder = order.GetHashCode();
                return View(numberOfOrder);
            }

            catch
            {
                return View("Error");
            }
        }
    }
}
