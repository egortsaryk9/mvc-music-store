﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMusicStore.Models;
using MvcMusicStore.Services;
using MvcMusicStore.Services.Home;
using MvcMusicStore.ViewModels.HomeViewModels;

namespace MvcMusicStore.Controllers
{
    public class HomeController : Controller
    {
        private HomeService _service;

        public HomeController(HomeService service)
        {
            this._service = service;
        }

        public ActionResult Index()
        {
            var indexViewModel = this._service.GetIndexViewModel();
            return View(indexViewModel);
        }
    }
}
