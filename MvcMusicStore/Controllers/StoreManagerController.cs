﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using MvcMusicStore.Documents;
using MvcMusicStore.Models;
using System.Data;
using System.Data.Entity;
using MvcMusicStore.MongoDB;
using MvcMusicStore.Services.StoreManager;
using MvcMusicStore.ViewModels.StoreManagerViewModels;

namespace MvcMusicStore.Controllers
{
    [Authorize]
    public class StoreManagerController : Controller
    {
        private StoreManagerService _service;

        public StoreManagerController(StoreManagerService service)
        {
            this._service = service;
        }
        
        public ViewResult Index()
        {
            var listOfViewModels = this._service.GetIndexViewModel();
            return View(listOfViewModels);
        }

        public ViewResult Details(string id)
        {
            var albumViewModel = this._service.GetDetailsViewModel(id);
            return View(albumViewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var createViewModel = this._service.GetCreateViewModel();
            return View(createViewModel);
        }


        [HttpPost]
        public ActionResult Create(StoreManagerCreateViewModel dataForCreating)
        {
            if (ModelState.IsValid)
            {
                this._service.CreateNewAlbum(dataForCreating);
                return RedirectToAction("Index");
            }

            var createViewModel = this._service.GetCreateViewModel();
            return View(createViewModel);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var editViewModel = this._service.GetEditViewModel(id);
            return View(editViewModel);
        }

        [HttpPost]
        public ActionResult Edit(StoreManagerEditViewModel dataForUpdating)
        {
            if (ModelState.IsValid)
            {
                this._service.EditAlbum(dataForUpdating);
                return RedirectToAction("Index");
            }

            return View(dataForUpdating);
        }

        [HttpGet]
        public ActionResult Delete(string id)
        {
            var deleteViewModel = this._service.GetDeleteViewModel(id);
            return View(deleteViewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            this._service.ConfirmDelete(id);
            return RedirectToAction("Index");
        }

    }

}