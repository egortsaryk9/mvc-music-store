﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcMusicStore.Documents;
using MvcMusicStore.Models;
using MvcMusicStore.Services.ShoppingCart;
using MvcMusicStore.ViewModels;
using MvcMusicStore.ViewModels.ShoppingCartViewModels;

namespace MvcMusicStore.Controllers
{
    public class ShoppingCartController : Controller
    {
        private ShoppingCartService _service;

        public ShoppingCartController(ShoppingCartService service)
        {
            this._service = service;
        }


        public ActionResult Index()
        {
            var indexViewModel = this._service.GetIndexViewModel(HttpContext.User.Identity.Name);
            return View(indexViewModel);
        }

        [Authorize]
        public ActionResult AddToCart(string id)
        {
            this._service.AddToCart(id, this.HttpContext.User.Identity.Name);
            return RedirectToAction("Index");
        }

        //
        // AJAX: /ShoppingCart/RemoveFromCart/"554776586754h5k455ggtge"
        [HttpPost]
        public ActionResult RemoveFromCart(string id)
        {
            var results = this._service.RemoveFromCart(id, this.HttpContext.User.Identity.Name);
            return Json(results);
        }

        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            if (String.IsNullOrEmpty(this.HttpContext.User.Identity.Name))
            {
                var summaryViewModel = this._service.GetEmptyCart();
                return PartialView(summaryViewModel);
            }

            else
            {
                var summaryViewModel = this._service.GetCartSummaryViewModel(this.HttpContext.User.Identity.Name);
                return PartialView(summaryViewModel);
            }
        }
    }
}
