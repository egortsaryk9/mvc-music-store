﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels.StoreManagerViewModels
{
    public class StoreManagerDetailsViewModel
    {
        public StoreManagerDetailsViewModel(AlbumDocument album)
        {
            Title = album.Title;
            GenreName = album.GenreName;
            ArtistName = album.ArtistName;
            Price = album.Price;
            AlbumArtUrl = album.AlbumArtUrl;
            Id = album.Id;
        }
        
        public string Title { get; set; }
        public string GenreName { get; set; }
        public string ArtistName { get; set; }
        public float Price { get; set; }
        public string AlbumArtUrl { get; set; }
        public string Id { get; set; }
    }
}