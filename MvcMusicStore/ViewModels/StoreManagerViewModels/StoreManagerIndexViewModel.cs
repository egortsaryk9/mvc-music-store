﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels.StoreManagerViewModels
{
    public class StoreManagerIndexViewModel
    {
        public StoreManagerIndexViewModel(AlbumDocument album)
        {
            GenreName = album.GenreName;
            ArtistName = album.ArtistName;
            Title = album.Title;
            Price = album.Price;
            Id = album.Id;
        }

        public string GenreName { get; set; }
        public string ArtistName { get; set; }
        public string Title { get; set; }
        public float Price { get; set; }
        public string Id { get; set; }
    }
}