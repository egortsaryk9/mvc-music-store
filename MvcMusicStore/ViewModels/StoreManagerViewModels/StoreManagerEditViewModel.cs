﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore.ViewModels.StoreManagerViewModels
{
    public class StoreManagerEditViewModel
    {
        [Required(ErrorMessage = "An album Title is required")]
        [StringLength(160)]
        public string Title { get; set; }

        [Required(ErrorMessage = "A Genre is required")]
        [DisplayName("Genre")]
        public string GenreName { get; set; }

        [Required(ErrorMessage = "An Artist is required")]
        [DisplayName("Artist")]
        public string ArtistName { get; set; }

        [Required(ErrorMessage = "A Price is required")]
        [Range(0.01, 100.00, ErrorMessage = "Price must be between 0.01 an 100.00")]
        public float Price { get; set; }

        public string AlbumArtUrl { get; set; }

        public SelectList ListOfGenres { get; set; }
        public SelectList ListOfArtists { get; set; }

        public string Id { get; set; }
    }
}