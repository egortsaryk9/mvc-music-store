﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMusicStore.ViewModels.StoreManagerViewModels
{
    public class StoreManagerDeleteViewModel
    {
        public StoreManagerDeleteViewModel(string title)
        {
            Title = title;
        }

        public string Title { get; set; }
    }
}