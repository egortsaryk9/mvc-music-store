﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels.HomeViewModels
{
    public class HomeIndexViewModel
    {
        public HomeIndexViewModel(AlbumDocument doc)
        {
            Title = doc.Title;
            Id = doc.Id;
            AlbumArtUrl = doc.AlbumArtUrl;
        }
        
        public string Title { get; set; }
        public string Id { get; set; }
        public string AlbumArtUrl { get; set; }
    }
}