﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMusicStore.ViewModels.StoreViewModels
{
    public class StoreGenreMenuViewModel
    {
        public StoreGenreMenuViewModel(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}