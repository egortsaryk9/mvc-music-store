﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels.StoreViewModels
{
    public class StoreDetailsViewModel
    {
        public StoreDetailsViewModel(AlbumDocument doc)
        {
            Title = doc.Title;
            AlbumArtUrl = doc.AlbumArtUrl;
            GenreName = doc.GenreName;
            ArtistName = doc.ArtistName;
            Price = doc.Price;
            Id = doc.Id;
        }

        public string Title { get; set; }
        public string AlbumArtUrl { get; set; }
        public string GenreName { get; set; }
        public string ArtistName { get; set; }
        public float Price { get; set; }
        public string Id { get; set; }

    }
}