﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels.StoreViewModels
{
    public class StoreBrowseViewModel
    {
        public StoreBrowseViewModel(string name, IEnumerable<StoreBrowseAlbumViewModel> albums)
        {
            Name = name;
            Albums = albums;
        }

        public string Name { get; set; }
        public IEnumerable<StoreBrowseAlbumViewModel> Albums { get; set; } 
    }
    

    public class StoreBrowseAlbumViewModel
    {
        public StoreBrowseAlbumViewModel(AlbumDocument doc)
        {
            Title = doc.Title;
            AlbumArtUrl = doc.AlbumArtUrl;
            Id = doc.Id;
        }

        public string Title { get; set; }
        public string AlbumArtUrl { get; set; }
        public string Id { get; set; }
    }
}