﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels
{
    public class ShoppingCartRemoveViewModel
    {
        public ShoppingCartRemoveViewModel(CartDocument cart, AlbumDocument album)
        {
            Message = album.Title + " has been removed from your cart";

            // Total price
            float totalPrice = 0;
            foreach (var purchase in cart.Purchase)
            {
                totalPrice += purchase.Price;
            }
            CartTotal = totalPrice;

            // Similar albums
            int itemCount = 0;
            foreach (var purchase in cart.Purchase)
            {
                if (purchase.Title == album.Title)
                {
                    itemCount += 1;
                }
            }
            ItemCount = itemCount;

            // Amount of purchases
            int amountOfPurchase = 0;
            foreach(var purchase in cart.Purchase)
            {
                amountOfPurchase += 1;
            }
            AmountOfPurchase = amountOfPurchase;

            DeleteId = album.Id;
        }

        public string Message { get; set; }
        public float CartTotal { get; set; } // total price
        public int ItemCount { get; set; } // amount of similar albums
        public string DeleteId { get; set; }
        public int AmountOfPurchase { get; set; } // total amount of purchases
    }
}