﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels.ShoppingCartViewModels
{
    public class ShoppingCartSummaryViewModel
    {
        public ShoppingCartSummaryViewModel(CartDocument cart)
        {
            var cartCount = 0;
            foreach (var purchase in cart.Purchase)
            {
                cartCount += 1;
            }
            
            CartCount = cartCount;
        }

        public int CartCount { get; set; }
    }
}