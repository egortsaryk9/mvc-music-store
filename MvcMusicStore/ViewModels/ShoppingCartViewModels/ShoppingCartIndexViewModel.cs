﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcMusicStore.Documents;

namespace MvcMusicStore.ViewModels.ShoppingCartViewModels
{
    public class ShoppingCartIndexViewModel
    {
        public ShoppingCartIndexViewModel(IEnumerable<ShoppingCartViewModel> albums, IEnumerable<AlbumDocument> allPurchases)
        {
            Albums = albums;

            float totalPrice = 0;
            foreach (var purchase in allPurchases)
            {
                totalPrice += purchase.Price;
            }

            TotalPrice = totalPrice;
            VariousTitles = new List<string>();
        }
        
        public float TotalPrice { get; set; }
        public IEnumerable<ShoppingCartViewModel> Albums { get; set; }
        public List<string> VariousTitles { get; set; } 
    }

    public class ShoppingCartViewModel
    {

        public ShoppingCartViewModel(AlbumDocument purchase)
        {
            Title = purchase.Title;
            AlbumId = purchase.Id;
            Price = purchase.Price;
        }
        
        public string Title { get; set; }
        public string AlbumId { get; set; }
        public float Price { get; set; }
        
    }
}